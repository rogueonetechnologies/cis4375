require 'test_helper'

class ProductQtiesControllerTest < ActionController::TestCase
  setup do
    @product_qty = product_qties(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:product_qties)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create product_qty" do
    assert_difference('ProductQty.count') do
      post :create, product_qty: { deleted_at: @product_qty.deleted_at, description: @product_qty.description, product_id: @product_qty.product_id, qty_type_id: @product_qty.qty_type_id }
    end

    assert_redirected_to product_qty_path(assigns(:product_qty))
  end

  test "should show product_qty" do
    get :show, id: @product_qty
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @product_qty
    assert_response :success
  end

  test "should update product_qty" do
    patch :update, id: @product_qty, product_qty: { deleted_at: @product_qty.deleted_at, description: @product_qty.description, product_id: @product_qty.product_id, qty_type_id: @product_qty.qty_type_id }
    assert_redirected_to product_qty_path(assigns(:product_qty))
  end

  test "should destroy product_qty" do
    assert_difference('ProductQty.count', -1) do
      delete :destroy, id: @product_qty
    end

    assert_redirected_to product_qties_path
  end
end
