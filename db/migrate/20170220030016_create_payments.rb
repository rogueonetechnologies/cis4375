class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.integer :payment_type_id
      t.integer :purchase_order_id
      t.date :payment_date
      t.float :payment_amt

      t.timestamps null: false
    end
  end
end
