class CreateSalesOrders < ActiveRecord::Migration
  def change
    create_table :sales_orders do |t|
      t.integer :customer_id
      t.integer :schedule_id
      t.integer :sales_order_status_id
      t.integer :sales_person_id
      t.date :order_date
      t.float :order_grand_total
      t.date :order_due_in_full_date
      t.boolean :is_approved
      t.date :order_balance_due
      t.date :order_balance_received
      t.datetime :deleted_at

      t.timestamps null: false
    end

    add_index :sales_orders, :deleted_at
  end
end
