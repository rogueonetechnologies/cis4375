# This migration was auto-generated via `rake db:generate_trigger_migration'.
# While you can edit this file, any changes you make to the definitions here
# will be undone by the next auto-generated trigger migration.

class CreateTriggersPaymentsInsertOrPaymentsUpdateOrPaymentsDeleteOrSalesOrdersInsert < ActiveRecord::Migration
  def up
    create_trigger("payments_after_insert_row_tr", :generated => true, :compatibility => 1).
        on("payments").
        after(:insert) do
      "UPDATE sales_orders SET order_balance_received = order_balance_received + NEW.payment_amt WHERE sales_orders.id = NEW.sales_order_id;"
    end

    create_trigger("payments_after_insert_row_tr_due", :generated => true, :compatibility => 1).
        on("payments").
        after(:insert) do
      "UPDATE sales_orders SET order_balance_due = order_balance_due - NEW.payment_amt WHERE sales_orders.id = NEW.sales_order_id;"
    end

    create_trigger("payments_after_update_of_payment_amt_row_tr", :generated => true, :compatibility => 1).
        on("payments").
        after(:update).
        of(:payment_amt) do
      "UPDATE sales_orders SET order_balance_received = order_balance_received + ( NEW.payment_amt - OLD.payment_amt ) WHERE sales_orders.id = NEW.sales_order_id;"
    end

    create_trigger("payments_after_update_of_payment_amt_row_tr_due", :generated => true, :compatibility => 1).
        on("payments").
        after(:update).
        of(:payment_amt) do
      "UPDATE sales_orders SET order_balance_due = order_balance_due - ( NEW.payment_amt - OLD.payment_amt ) WHERE sales_orders.id = NEW.sales_order_id;"
    end

    create_trigger("payments_after_delete_row_tr", :generated => true, :compatibility => 1).
        on("payments").
        after(:delete) do
      "UPDATE sales_orders SET order_balance_received = order_balance_received - OLD.payment_amt WHERE sales_orders.id = OLD.sales_order_id;"
    end

    create_trigger("payments_after_delete_row_tr_due", :generated => true, :compatibility => 1).
        on("payments").
        after(:delete) do
      "UPDATE sales_orders SET order_balance_due = order_balance_due + OLD.payment_amt WHERE sales_orders.id = OLD.sales_order_id;"
    end

    create_trigger("sales_orders_after_insert_row_tr", :generated => true, :compatibility => 1).
        on("sales_orders").
        after(:insert) do
      "UPDATE sales_orders SET order_balance_due = order_grand_total WHERE sales_orders.id = NEW.id;"
    end
  end

  def down
    drop_trigger("payments_after_insert_row_tr", "payments", :generated => true)

    drop_trigger("payments_after_insert_row_tr_due", "payments", :generated => true)

    drop_trigger("payments_after_update_of_payment_amt_row_tr", "payments", :generated => true)

    drop_trigger("payments_after_update_of_payment_amt_row_tr_due", "payments", :generated => true)

    drop_trigger("payments_after_delete_row_tr", "payments", :generated => true)

    drop_trigger("payments_after_delete_row_tr_due", "payments", :generated => true)

    drop_trigger("sales_orders_after_insert_row_tr", "sales_orders", :generated => true)
  end
end
