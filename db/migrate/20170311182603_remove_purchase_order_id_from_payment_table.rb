class RemovePurchaseOrderIdFromPaymentTable < ActiveRecord::Migration
  def change
    remove_column :payments, :purchase_order_id
  end
end
