class CreateProductQties < ActiveRecord::Migration
  def change
    create_table :product_qties do |t|
      t.integer :product_id
      t.integer :qty_type_id
      t.string :description
      t.timestamp :deleted_at

      t.timestamps null: false
    end

    add_index :product_qties, :deleted_at
  end
end
