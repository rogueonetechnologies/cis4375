class AddSalesOrderIdToPayment < ActiveRecord::Migration
  def change
    add_column :payments, :sales_order_id, :integer
  end
end
