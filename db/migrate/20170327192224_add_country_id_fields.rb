class AddCountryIdFields < ActiveRecord::Migration
  def change
    add_column :sales_people, :country_id, :integer
    add_column :customers, :country_id, :integer
  end
end
