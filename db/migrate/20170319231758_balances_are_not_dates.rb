class BalancesAreNotDates < ActiveRecord::Migration
  def change
    remove_column :sales_orders, :order_balance_due
    add_column :sales_orders, :order_balance_due, :float

    remove_column :sales_orders, :order_balance_received
    add_column :sales_orders, :order_balance_received, :float
  end
end
