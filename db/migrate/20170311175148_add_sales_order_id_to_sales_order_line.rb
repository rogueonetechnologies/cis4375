class AddSalesOrderIdToSalesOrderLine < ActiveRecord::Migration
  def change

    add_column :sales_order_lines, :sales_order_id, :integer

  end
end
