class CreateQtyTypes < ActiveRecord::Migration
  def change
    create_table :qty_types do |t|
      t.string :qty_type_name
      t.timestamp :deleted_at

      t.timestamps null: false
    end

    add_index :qty_types, :deleted_at
  end
end
