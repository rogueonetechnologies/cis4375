class RenameScheduleId < ActiveRecord::Migration
  def change
    rename_column :sales_orders, :schedule_id, :payment_schedule_id
  end
end
