class CreateCustomerStatuses < ActiveRecord::Migration
  def change
    create_table :customer_statuses do |t|
      t.string :status_desc

      t.timestamps null: false
    end
  end
end
