class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.integer :customer_id
      t.string :contact_name
      t.string :phone_number
      t.string :email

      t.timestamps null: false
    end
  end
end
