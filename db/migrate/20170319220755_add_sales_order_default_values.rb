class AddSalesOrderDefaultValues < ActiveRecord::Migration
  def change
    change_column :sales_orders, :sales_order_status_id, :integer, :default => 1
  end
end
