class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.integer :customer_status_id
      t.integer :state_id
      t.integer :customer_type_id
      t.string :customer_name
      t.string :phone_number

      t.timestamps null: false
    end
  end
end
