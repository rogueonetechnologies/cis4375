class ChangeScheduleLengthType < ActiveRecord::Migration
  def change
    remove_column :payment_schedules, :schedule_length
    add_column :payment_schedules, :schedule_length, :integer
  end
end
