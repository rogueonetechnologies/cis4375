class RemoveCustomerTypeIdFromCustomers < ActiveRecord::Migration
  def change
    remove_column :customers, :customer_type_id
  end
end
