class AddBackProductId < ActiveRecord::Migration
  def change
    add_column :sales_order_lines, :product_id, :integer
  end
end
