class ChangeProductAsociation < ActiveRecord::Migration
  def change
    remove_column :sales_order_lines, :product_id
    add_column :sales_order_lines, :product_qty_id, :integer
  end
end