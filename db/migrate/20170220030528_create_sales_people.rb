class CreateSalesPeople < ActiveRecord::Migration
  def change
    create_table :sales_people do |t|
      t.integer :state_id
      t.string :first_name
      t.string :last_name

      t.timestamps null: false
    end
  end
end
