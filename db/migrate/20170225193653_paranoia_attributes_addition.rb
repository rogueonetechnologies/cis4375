class ParanoiaAttributesAddition < ActiveRecord::Migration
  def change
    add_column :contacts, :deleted_at, :datetime
    add_index :contacts, :deleted_at

    add_column :countries, :deleted_at, :datetime
    add_index :countries, :deleted_at

    add_column :customer_statuses, :deleted_at, :datetime
    add_index :customer_statuses, :deleted_at

    add_column :customers, :deleted_at, :datetime
    add_index :customers, :deleted_at

    add_column :payment_types, :deleted_at, :datetime
    add_index :payment_types, :deleted_at

    add_column :payments, :deleted_at, :datetime
    add_index :payments, :deleted_at

    add_column :products, :deleted_at, :datetime
    add_index :products, :deleted_at

    add_column :sales_people, :deleted_at, :datetime
    add_index :sales_people, :deleted_at

    add_column :states, :deleted_at, :datetime
    add_index :states, :deleted_at

    add_column :users, :deleted_at, :datetime
    add_index :users, :deleted_at

  end
end
