class CreateSalesOrderStatuses < ActiveRecord::Migration
  def change
    create_table :sales_order_statuses do |t|
      t.string :status_desc
      t.datetime :deleted_at

      t.timestamps null: false
    end

    add_index :sales_order_statuses, :deleted_at
  end
end
