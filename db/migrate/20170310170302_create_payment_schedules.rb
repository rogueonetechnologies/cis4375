class CreatePaymentSchedules < ActiveRecord::Migration
  def change
    create_table :payment_schedules do |t|
      t.string :schedule_length
      t.datetime :deleted_at

      t.timestamps null: false
    end

    add_index :payment_schedules, :deleted_at
  end
end
