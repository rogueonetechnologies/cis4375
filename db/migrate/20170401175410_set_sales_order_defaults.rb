class SetSalesOrderDefaults < ActiveRecord::Migration
  def change
    change_column :sales_orders, :order_balance_received, :float, default: 0.00
  end
end
