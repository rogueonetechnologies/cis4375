class CreateSalesOrderLines < ActiveRecord::Migration
  def change
    create_table :sales_order_lines do |t|
      t.integer :product_id
      t.integer :qty_requested
      t.integer :qty_approved
      t.float :unit_price
      t.float :product_total
      t.datetime :deleted_at

      t.timestamps null: false
    end

    add_index :sales_order_lines, :deleted_at
  end
end
