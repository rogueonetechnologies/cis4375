# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170412181709) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "contacts", force: :cascade do |t|
    t.integer  "customer_id"
    t.string   "contact_name"
    t.string   "phone_number"
    t.string   "email"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.datetime "deleted_at"
  end

  add_index "contacts", ["deleted_at"], name: "index_contacts_on_deleted_at", using: :btree

  create_table "countries", force: :cascade do |t|
    t.string   "country_name"
    t.string   "country_abbv"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.datetime "deleted_at"
  end

  add_index "countries", ["deleted_at"], name: "index_countries_on_deleted_at", using: :btree

  create_table "customer_statuses", force: :cascade do |t|
    t.string   "status_desc"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.datetime "deleted_at"
  end

  add_index "customer_statuses", ["deleted_at"], name: "index_customer_statuses_on_deleted_at", using: :btree

  create_table "customers", force: :cascade do |t|
    t.integer  "customer_status_id"
    t.integer  "state_id"
    t.string   "customer_name"
    t.string   "phone_number"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.datetime "deleted_at"
    t.integer  "country_id"
  end

  add_index "customers", ["deleted_at"], name: "index_customers_on_deleted_at", using: :btree

  create_table "payment_schedules", force: :cascade do |t|
    t.datetime "deleted_at"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "schedule_length"
  end

  add_index "payment_schedules", ["deleted_at"], name: "index_payment_schedules_on_deleted_at", using: :btree

  create_table "payment_types", force: :cascade do |t|
    t.string   "type_desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
  end

  add_index "payment_types", ["deleted_at"], name: "index_payment_types_on_deleted_at", using: :btree

  create_table "payments", force: :cascade do |t|
    t.integer  "payment_type_id"
    t.date     "payment_date"
    t.float    "payment_amt"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.datetime "deleted_at"
    t.integer  "sales_order_id"
  end

  add_index "payments", ["deleted_at"], name: "index_payments_on_deleted_at", using: :btree

  create_table "product_qties", force: :cascade do |t|
    t.integer  "product_id"
    t.integer  "qty_type_id"
    t.string   "description"
    t.datetime "deleted_at"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "product_qties", ["deleted_at"], name: "index_product_qties_on_deleted_at", using: :btree

  create_table "products", force: :cascade do |t|
    t.string   "product_name"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.datetime "deleted_at"
  end

  add_index "products", ["deleted_at"], name: "index_products_on_deleted_at", using: :btree

  create_table "qty_types", force: :cascade do |t|
    t.string   "qty_type_name"
    t.datetime "deleted_at"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "qty_types", ["deleted_at"], name: "index_qty_types_on_deleted_at", using: :btree

  create_table "sales_order_lines", force: :cascade do |t|
    t.integer  "qty_requested"
    t.integer  "qty_approved"
    t.float    "unit_price"
    t.float    "product_total"
    t.datetime "deleted_at"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "sales_order_id"
    t.integer  "product_qty_id"
    t.integer  "product_id"
  end

  add_index "sales_order_lines", ["deleted_at"], name: "index_sales_order_lines_on_deleted_at", using: :btree

  create_table "sales_order_statuses", force: :cascade do |t|
    t.string   "status_desc"
    t.datetime "deleted_at"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "sales_order_statuses", ["deleted_at"], name: "index_sales_order_statuses_on_deleted_at", using: :btree

  create_table "sales_orders", force: :cascade do |t|
    t.integer  "customer_id"
    t.integer  "payment_schedule_id"
    t.integer  "sales_order_status_id",  default: 1
    t.integer  "sales_person_id"
    t.date     "order_date"
    t.float    "order_grand_total"
    t.date     "order_due_in_full_date"
    t.boolean  "is_approved"
    t.datetime "deleted_at"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.float    "order_balance_due"
    t.float    "order_balance_received", default: 0.0
  end

  add_index "sales_orders", ["deleted_at"], name: "index_sales_orders_on_deleted_at", using: :btree

  create_table "sales_people", force: :cascade do |t|
    t.integer  "state_id"
    t.string   "first_name"
    t.string   "last_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.integer  "country_id"
  end

  add_index "sales_people", ["deleted_at"], name: "index_sales_people_on_deleted_at", using: :btree

  create_table "states", force: :cascade do |t|
    t.integer  "country_id"
    t.string   "state_name"
    t.string   "state_abbv"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
  end

  add_index "states", ["deleted_at"], name: "index_states_on_deleted_at", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "password"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "deleted_at"
    t.string   "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invitation_limit"
    t.integer  "invited_by_id"
    t.string   "invited_by_type"
    t.integer  "invitations_count",      default: 0
    t.boolean  "admin",                  default: false
  end

  add_index "users", ["deleted_at"], name: "index_users_on_deleted_at", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["invitation_token"], name: "index_users_on_invitation_token", unique: true, using: :btree
  add_index "users", ["invitations_count"], name: "index_users_on_invitations_count", using: :btree
  add_index "users", ["invited_by_id"], name: "index_users_on_invited_by_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_trigger("payments_after_insert_row_tr", :generated => true, :compatibility => 1).
      on("payments").
      after(:insert) do
    "UPDATE sales_orders SET order_balance_received = order_balance_received + NEW.payment_amt WHERE sales_orders.id = NEW.sales_order_id;"
  end

  create_trigger("payments_after_insert_row_tr_due", :generated => true, :compatibility => 1).
      on("payments").
      after(:insert) do
    "UPDATE sales_orders SET order_balance_due = order_balance_due - NEW.payment_amt WHERE sales_orders.id = NEW.sales_order_id;"
  end

  create_trigger("payments_after_update_of_payment_amt_row_tr", :generated => true, :compatibility => 1).
      on("payments").
      after(:update).
      of(:payment_amt) do
    "UPDATE sales_orders SET order_balance_received = order_balance_received + ( NEW.payment_amt - OLD.payment_amt ) WHERE sales_orders.id = NEW.sales_order_id;"
  end

  create_trigger("payments_after_update_of_payment_amt_row_tr_due", :generated => true, :compatibility => 1).
      on("payments").
      after(:update).
      of(:payment_amt) do
    "UPDATE sales_orders SET order_balance_due = order_balance_due - ( NEW.payment_amt - OLD.payment_amt ) WHERE sales_orders.id = NEW.sales_order_id;"
  end

  create_trigger("payments_after_delete_row_tr", :generated => true, :compatibility => 1).
      on("payments").
      after(:delete) do
    "UPDATE sales_orders SET order_balance_received = order_balance_received - OLD.payment_amt WHERE sales_orders.id = OLD.sales_order_id;"
  end

  create_trigger("payments_after_delete_row_tr_due", :generated => true, :compatibility => 1).
      on("payments").
      after(:delete) do
    "UPDATE sales_orders SET order_balance_due = order_balance_due + OLD.payment_amt WHERE sales_orders.id = OLD.sales_order_id;"
  end

  create_trigger("sales_orders_after_insert_row_tr", :generated => true, :compatibility => 1).
      on("sales_orders").
      after(:insert) do
    "UPDATE sales_orders SET order_balance_due = order_grand_total WHERE sales_orders.id = NEW.id;"
  end

end
