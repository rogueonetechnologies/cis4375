Country.delete_all
State.delete_all
CustomerStatus.delete_all
PaymentSchedule.delete_all
PaymentType.delete_all
Product.delete_all
QtyType.delete_all
ProductQty.delete_all
SalesOrderStatus.delete_all
SalesOrder.delete_all
Contact.delete_all
Payment.delete_all
SalesOrderLine.delete_all
Customer.delete_all
SalesPerson.delete_all
User.delete_all

require 'csv'

CSV.foreach('db/csv/01_country.csv', {encoding: 'UTF-8', headers: true, header_converters: :symbol, converters: :all}) do |row|
  Country.create(row.to_hash)
end

CSV.foreach('db/csv/02_state.csv', {encoding: 'UTF-8', headers: true, header_converters: :symbol, converters: :all}) do |row|
  State.create(row.to_hash)
end

CSV.foreach('db/csv/03_customer_status.csv', {encoding: 'UTF-8', headers: true, header_converters: :symbol, converters: :all}) do |row|
  CustomerStatus.create(row.to_hash)
end

CSV.foreach('db/csv/04_payment_schedule.csv', {encoding: 'UTF-8', headers: true, header_converters: :symbol, converters: :all}) do |row|
  PaymentSchedule.create(row.to_hash)
end

CSV.foreach('db/csv/05_payment_type.csv', {encoding: 'UTF-8', headers: true, header_converters: :symbol, converters: :all}) do |row|
  PaymentType.create(row.to_hash)
end

CSV.foreach('db/csv/06_product.csv', {encoding: 'UTF-8', headers: true, header_converters: :symbol, converters: :all}) do |row|
  Product.create(row.to_hash)
end

CSV.foreach('db/csv/07_qty_type.csv', {encoding: 'UTF-8', headers: true, header_converters: :symbol, converters: :all}) do |row|
  QtyType.create(row.to_hash)
end

CSV.foreach('db/csv/08_product_qty.csv', {encoding: 'UTF-8', headers: true, header_converters: :symbol, converters: :all}) do |row|
  ProductQty.create(row.to_hash)
end

CSV.foreach('db/csv/09_sales_order_status.csv', {encoding: 'UTF-8', headers: true, header_converters: :symbol, converters: :all}) do |row|
  SalesOrderStatus.create(row.to_hash)
end

CSV.foreach('db/csv/10_customer.csv', {encoding: 'UTF-8', headers: true, header_converters: :symbol, converters: :all}) do |row|
  Customer.create(row.to_hash)
end

CSV.foreach('db/csv/11_sales_person.csv', {encoding: 'UTF-8', headers: true, header_converters: :symbol, converters: :all}) do |row|
  SalesPerson.create(row.to_hash)
end

CSV.foreach('db/csv/12_sales_order.csv', {encoding: 'UTF-8', headers: true, header_converters: :symbol, converters: :all}) do |row|
  SalesOrder.create(row.to_hash)
end

CSV.foreach('db/csv/13_sales_order_line.csv', {encoding: 'UTF-8', headers: true, header_converters: :symbol, converters: :all}) do |row|
  SalesOrderLine.create(row.to_hash)
end

CSV.foreach('db/csv/14_payment.csv', {encoding: 'UTF-8', headers: true, header_converters: :symbol, converters: :all}) do |row|
  Payment.create(row.to_hash)
end

CSV.foreach('db/csv/15_contact.csv', {encoding: 'UTF-8', headers: true, header_converters: :symbol, converters: :all}) do |row|
  Contact.create(row.to_hash)
end

User.create!([{email: "jebradham@uh.edu", password: "someone", admin: true},
              {email: "orcauser@oceanagro.com", password: "orca123", admin: false}
])