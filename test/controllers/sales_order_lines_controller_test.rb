require 'test_helper'

class SalesOrderLinesControllerTest < ActionController::TestCase
  setup do
    @sales_order_line = sales_order_lines(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sales_order_lines)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sales_order_line" do
    assert_difference('SalesOrderLine.count') do
      post :create, sales_order_line: { deleted_at: @sales_order_line.deleted_at, product_id: @sales_order_line.product_id, product_total: @sales_order_line.product_total, qty_approved: @sales_order_line.qty_approved, qty_requested: @sales_order_line.qty_requested, unit_price: @sales_order_line.unit_price }
    end

    assert_redirected_to sales_order_line_path(assigns(:sales_order_line))
  end

  test "should show sales_order_line" do
    get :show, id: @sales_order_line
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sales_order_line
    assert_response :success
  end

  test "should update sales_order_line" do
    patch :update, id: @sales_order_line, sales_order_line: { deleted_at: @sales_order_line.deleted_at, product_id: @sales_order_line.product_id, product_total: @sales_order_line.product_total, qty_approved: @sales_order_line.qty_approved, qty_requested: @sales_order_line.qty_requested, unit_price: @sales_order_line.unit_price }
    assert_redirected_to sales_order_line_path(assigns(:sales_order_line))
  end

  test "should destroy sales_order_line" do
    assert_difference('SalesOrderLine.count', -1) do
      delete :destroy, id: @sales_order_line
    end

    assert_redirected_to sales_order_lines_path
  end
end
