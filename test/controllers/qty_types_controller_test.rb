require 'test_helper'

class QtyTypesControllerTest < ActionController::TestCase
  setup do
    @qty_type = qty_types(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:qty_types)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create qty_type" do
    assert_difference('QtyType.count') do
      post :create, qty_type: { deleted_at: @qty_type.deleted_at, qty_type_name: @qty_type.qty_type_name }
    end

    assert_redirected_to qty_type_path(assigns(:qty_type))
  end

  test "should show qty_type" do
    get :show, id: @qty_type
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @qty_type
    assert_response :success
  end

  test "should update qty_type" do
    patch :update, id: @qty_type, qty_type: { deleted_at: @qty_type.deleted_at, qty_type_name: @qty_type.qty_type_name }
    assert_redirected_to qty_type_path(assigns(:qty_type))
  end

  test "should destroy qty_type" do
    assert_difference('QtyType.count', -1) do
      delete :destroy, id: @qty_type
    end

    assert_redirected_to qty_types_path
  end
end
