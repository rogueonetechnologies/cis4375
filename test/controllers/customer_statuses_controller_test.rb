require 'test_helper'

class CustomerStatusesControllerTest < ActionController::TestCase
  setup do
    @customer_status = customer_statuses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:customer_statuses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create customer_status" do
    assert_difference('CustomerStatus.count') do
      post :create, customer_status: { status_desc: @customer_status.status_desc }
    end

    assert_redirected_to customer_status_path(assigns(:customer_status))
  end

  test "should show customer_status" do
    get :show, id: @customer_status
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @customer_status
    assert_response :success
  end

  test "should update customer_status" do
    patch :update, id: @customer_status, customer_status: { status_desc: @customer_status.status_desc }
    assert_redirected_to customer_status_path(assigns(:customer_status))
  end

  test "should destroy customer_status" do
    assert_difference('CustomerStatus.count', -1) do
      delete :destroy, id: @customer_status
    end

    assert_redirected_to customer_statuses_path
  end
end
