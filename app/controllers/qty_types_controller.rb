class QtyTypesController < ApplicationController
  before_action :set_qty_type, only: [:show, :edit, :update, :destroy, :hard_delete, :restore]

  # GET /qty_types
  # GET /qty_types.json
  def index
    @qty_types = QtyType.paginate(:page => params[:page])
  end

  # GET /qty_type/deleted
  def deleted
    @qty_types = QtyType.only_deleted.order(qty_type_name: :asc).paginate(:page => params[:page])
  end

  # POST /qty_type/:id/delete
  def hard_delete
    @qty_type.really_destroy!
    redirect_to deleted_qty_types_path, notice: 'Quantity Type permanently deleted'
  end

  # POST /qty_type/:id/reinstate
  def restore
    @qty_type.restore
    redirect_to deleted_qty_types_path, notice: 'Quantity Type reinstated'
  end

  # GET /qty_types/1
  # GET /qty_types/1.json
  def show
  end

  # GET /qty_types/new
  def new
    @qty_type = QtyType.new
  end

  # GET /qty_types/1/edit
  def edit
  end

  # POST /qty_types
  # POST /qty_types.json
  def create
    @qty_type = QtyType.new(qty_type_params)

    respond_to do |format|
      if @qty_type.save
        format.html { redirect_to @qty_type, notice: 'Qty type was successfully created.' }
        format.json { render :show, status: :created, location: @qty_type }
      else
        format.html { render :new }
        format.json { render json: @qty_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /qty_types/1
  # PATCH/PUT /qty_types/1.json
  def update
    respond_to do |format|
      if @qty_type.update(qty_type_params)
        format.html { redirect_to @qty_type, notice: 'Qty type was successfully updated.' }
        format.json { render :show, status: :ok, location: @qty_type }
      else
        format.html { render :edit }
        format.json { render json: @qty_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /qty_types/1
  # DELETE /qty_types/1.json
  def destroy
    @qty_type.destroy
    respond_to do |format|
      format.html { redirect_to qty_types_url, notice: 'Qty type was successfully removed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_qty_type
      @qty_type = QtyType.with_deleted.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def qty_type_params
      params.require(:qty_type).permit(:qty_type_name, :deleted_at)
    end
end
