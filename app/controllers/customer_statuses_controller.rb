class CustomerStatusesController < ApplicationController
  before_action :set_customer_status, only: [:show, :edit, :update, :destroy, :hard_delete, :restore]
  before_action :authenticate_user!

  # GET /customer_statuses
  # GET /customer_statuses.json
  def index
    @customer_statuses = CustomerStatus.paginate(:page => params[:page])
  end

  # GET /customer_status/deleted
  def deleted
    @customer_statuses = CustomerStatus.only_deleted.paginate(:page => params[:page])
  end

  # POST /customer_status/:id/delete
  def hard_delete
    @customer_status.really_destroy!
    redirect_to deleted_customer_statuses_path, notice: 'Customer status permanently deleted'
  end

  # POST /customer_status/:id/reinstate
  def restore
    @customer_status.restore
    redirect_to deleted_customer_statuses_path, notice: 'Customer status reinstated'
  end

  # GET /customer_statuses/1
  # GET /customer_statuses/1.json
  def show
  end

  # GET /customer_statuses/new
  def new
    @customer_status = CustomerStatus.new
  end

  # GET /customer_statuses/1/edit
  def edit
  end

  # POST /customer_statuses
  # POST /customer_statuses.json
  def create
    @customer_status = CustomerStatus.new(customer_status_params)

    respond_to do |format|
      if @customer_status.save
        format.html { redirect_to @customer_status, notice: 'Customer status was successfully created.' }
        format.json { render :show, status: :created, location: @customer_status }
      else
        format.html { render :new }
        format.json { render json: @customer_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /customer_statuses/1
  # PATCH/PUT /customer_statuses/1.json
  def update
    respond_to do |format|
      if @customer_status.update(customer_status_params)
        format.html { redirect_to @customer_status, notice: 'Customer status was successfully updated.' }
        format.json { render :show, status: :ok, location: @customer_status }
      else
        format.html { render :edit }
        format.json { render json: @customer_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /customer_statuses/1
  # DELETE /customer_statuses/1.json
  def destroy
    @customer_status.destroy
    respond_to do |format|
      format.html { redirect_to customer_statuses_url, notice: 'Customer status was successfully removed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_customer_status
      @customer_status = CustomerStatus.with_deleted.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def customer_status_params
      params.require(:customer_status).permit(:status_desc)
    end
end
