class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :authenticate_user!

  helper_method :to_indian_currency

  def to_indian_currency(number)
    number = '%.2f' % number
    "₹#{number.to_s.gsub(/(\d+?)(?=(\d\d)+(\d)(?!\d))(\.\d+)?/, "\\1,")}"
  end

  private
  def authenticate_admin!
    unless current_user.admin
      redirect_to root_url, :alert => "Access Denied: Only admin users can access this area."
    end
  end
end
