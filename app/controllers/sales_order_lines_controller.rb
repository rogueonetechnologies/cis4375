class SalesOrderLinesController < ApplicationController
  before_action :set_sales_order_line, only: [:show, :edit, :update, :destroy, :hard_delete, :restore]

  # GET /sales_order_lines
  # GET /sales_order_lines.json
  def index
    @sales_order_lines = SalesOrderLine.paginate(:page => params[:page])
  end

  # GET /sales_order_line/deleted
  def deleted
    @sales_order_lines = SalesOrderLine.only_deleted.paginate(:page => params[:page])
  end

  # POST /sales_order_line/:id/delete
  def hard_delete
    @sales_order_line.really_destroy!
    redirect_to deleted_sales_order_lines_path, notice: 'Sales Order Line permanently deleted'
  end

  # POST /sales_order_line/:id/reinstate
  def restore
    @sales_order_line.restore
    redirect_to deleted_sales_order_lines_path, notice: 'Sales Order Line reinstated'
  end

  # GET /sales_order_lines/1
  # GET /sales_order_lines/1.json
  def show
  end

  # GET /sales_order_lines/new
  def new
    @sales_order_line = SalesOrderLine.new
  end

  # GET /sales_order_lines/1/edit
  def edit
  end

  # POST /sales_order_lines
  # POST /sales_order_lines.json
  def create
    @sales_order_line = SalesOrderLine.new(sales_order_line_params)

    respond_to do |format|
      if @sales_order_line.save
        format.html { redirect_to @sales_order_line, notice: 'Sales Order line was successfully created.' }
        format.json { render :show, status: :created, location: @sales_order_line }
      else
        format.html { render :new }
        format.json { render json: @sales_order_line.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sales_order_lines/1
  # PATCH/PUT /sales_order_lines/1.json
  def update
    respond_to do |format|
      if @sales_order_line.update(sales_order_line_params)
        format.html { redirect_to @sales_order_line, notice: 'Sales Order line was successfully updated.' }
        format.json { render :show, status: :ok, location: @sales_order_line }
      else
        format.html { render :edit }
        format.json { render json: @sales_order_line.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sales_order_lines/1
  # DELETE /sales_order_lines/1.json
  def destroy
    @sales_order_line.destroy
    respond_to do |format|
      format.html { redirect_to sales_order_lines_url, notice: 'Sales Order line was successfully removed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sales_order_line
      @sales_order_line = SalesOrderLine.with_deleted.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sales_order_line_params
      params.require(:sales_order_line).permit(:product_id, :qty_requested, :qty_approved, :unit_price, :product_total, :deleted_at)
    end
end
