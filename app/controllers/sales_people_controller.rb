class SalesPeopleController < ApplicationController
  before_action :set_sales_person, only: [:show, :edit, :update, :destroy, :hard_delete, :restore]
  before_action :authenticate_user!
  before_action :authenticate_admin!, only: [:deleted, :hard_delete, :restore]

  # GET /sales_people
  # GET /sales_people.json
  def index
    @sales_people = SalesPerson.order(last_name: :asc, first_name: :asc).paginate(:page => params[:page])
    @all_sales_people = SalesPerson.all
    respond_to do |format|
      format.html
      format.xlsx {
        response.headers['Content-Disposition'] = 'attachment; filename="ORCA_Sales_Person_List_'+Date.today.to_s+'.xlsx"'
      }
    end
  end
  
  def deleted
    @sales_people = SalesPerson.only_deleted.paginate(:page => params[:page])
  end

  # POST /sales_person/:id/delete
  def hard_delete
    @sales_person.really_destroy!
    redirect_to deleted_sales_people_path, notice: 'Salesperson permanently deleted'
  end

  # POST /sales_person/:id/reinstate
  def restore
    @sales_person.restore
    redirect_to deleted_sales_people_path, notice: 'Salesperson reinstated'
  end

  # GET /sales_people/1
  # GET /sales_people/1.json
  def show
    @sales_orders = SalesOrder.where(:sales_person_id => @sales_person.id).paginate(:page => params[:page], :per_page => 10)
  end

  # GET /sales_people/new
  def new
    @countries = Country.joins(:states).distinct
    @sales_person = SalesPerson.new
  end

  # GET /sales_people/1/edit
  def edit
    @countries = Country.joins(:states).distinct
  end

  # POST /sales_people
  # POST /sales_people.json
  def create
    @countries = Country.joins(:states).group('countries.id')
    @sales_person = SalesPerson.new(sales_person_params)

    respond_to do |format|
      if @sales_person.save
        format.html { redirect_to @sales_person, notice: 'Sales person was successfully created.' }
        format.json { render :show, status: :created, location: @sales_person }
      else
        format.html { render :new }
        format.json { render json: @sales_person.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sales_people/1
  # PATCH/PUT /sales_people/1.json
  def update
    respond_to do |format|
      if @sales_person.update(sales_person_params)
        format.html { redirect_to @sales_person, notice: 'Sales person was successfully updated.' }
        format.json { render :show, status: :ok, location: @sales_person }
      else
        format.html { render :edit }
        format.json { render json: @sales_person.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sales_people/1
  # DELETE /sales_people/1.json
  def destroy
    @sales_person.destroy
    respond_to do |format|
      format.html { redirect_to sales_people_url, notice: 'Sales person was successfully removed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sales_person
      @sales_person = SalesPerson.with_deleted.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sales_person_params
      params.require(:sales_person).permit(:state_id, :country_id, :first_name, :last_name)
    end
end
