class StatesController < ApplicationController
  before_action :set_state, only: [:show, :edit, :update, :destroy, :hard_delete, :restore]
  before_action :authenticate_admin!, only: [:deleted, :hard_delete, :restore]

  # GET /states
  # GET /states.json
  def index
    @states = State.order(country_id: :asc, state_name: :asc).paginate(:page => params[:page])
  end

  # GET /state/deleted
  def deleted
    @states = State.only_deleted.order(state_name: :asc).paginate(:page => params[:page])
  end

  # POST /state/:id/delete
  def hard_delete
    @state.really_destroy!
    redirect_to deleted_states_path, notice: 'State permanently deleted'
  end

  # POST /state/:id/reinstate
  def restore
    @state.restore
    redirect_to deleted_states_path, notice: 'State reinstated'
  end

  # GET /states/1
  # GET /states/1.json
  def show
  end

  # GET /states/new
  def new
    @state = State.new
  end

  # GET /states/1/edit
  def edit
  end

  # POST /states
  # POST /states.json
  def create
    @state = State.new(state_params)

    respond_to do |format|
      if @state.save
        format.html { redirect_to @state, notice: 'State was successfully created.' }
        format.json { render :show, status: :created, location: @state }
      else
        format.html { render :new }
        format.json { render json: @state.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /states/1
  # PATCH/PUT /states/1.json
  def update
    respond_to do |format|
      if @state.update(state_params)
        format.html { redirect_to @state, notice: 'State was successfully updated.' }
        format.json { render :show, status: :ok, location: @state }
      else
        format.html { render :edit }
        format.json { render json: @state.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /states/1
  # DELETE /states/1.json
  def destroy
    @state.destroy
    respond_to do |format|
      format.html { redirect_to states_url, notice: 'State was successfully removed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_state
      @state = State.with_deleted.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def state_params
      params.require(:state).permit(:country_id, :state_name, :state_abbv)
    end
end
