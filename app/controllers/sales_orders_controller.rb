class SalesOrdersController < ApplicationController
  before_action :set_sales_order, only: [:show, :edit, :update, :destroy, :hard_delete, :restore, :ajax]
  before_action :authenticate_admin!, only: [:deleted, :hard_delete, :restore]

  # GET /sales_orders
  # GET /sales_orders.json
  def index
    @q = SalesOrder.ransack(params[:q])
    @sales_orders = @q.result.paginate(:page => params[:page])
    @all_sos = SalesOrder.all
    respond_to do |format|
      format.html
      format.xlsx {
        response.headers['Content-Disposition'] = 'attachment; filename="ORCA_Sales_Order_List_'+Date.today.to_s+'.xlsx"'
      }
    end
  end

  def ajax
    respond_to do |format|
      format.html { render :layout => false } # your-action.html.erb
    end
  end

  def ytd_sales
    year = Date.today
    @ytd_q = SalesOrder.where('extract(year  from order_date) = ? AND is_approved = ?', year.year, true).ransack(params[:q])
    @ytd_sales = @ytd_q.result.paginate(:page => params[:page])
    respond_to do |format|
      format.html
      format.xlsx {
        if params[:state]
          @ytd_exp = SalesOrder.joins(:customer).where('extract(year  from order_date) = ? AND is_approved = ? AND state_id = ?', year.year, true, params[:state])
          state = State.select(:state_name).where(:id => params[:state]).first.state_name
        else
          @ytd_exp = @ytd_q.result
          state = 'All_States'
        end
        response.headers['Content-Disposition'] = 'attachment; filename="ORCA_'+state+'_YTD_Sales_'+Date.today.to_s+'.xlsx"'
      }
    end
  end

  # GET /sales_order/deleted
  def deleted
    @sales_orders = SalesOrder.only_deleted.paginate(:page => params[:page])
  end

  # POST /sales_order/:id/delete
  def hard_delete
    @sales_order.really_destroy!
    redirect_to deleted_sales_orders_path, notice: 'Sales Order permanently deleted'
  end

  # POST /sales_order/:id/reinstate
  def restore
    @sales_order.restore
    redirect_to deleted_sales_orders_path, notice: 'Sales Order reinstated'
  end

  # GET /sales_orders/1
  # GET /sales_orders/1.json
  def show
  end

  # GET /sales_orders/new
  def new
    @sales_order = SalesOrder.new
  end

  # GET /sales_orders/1/edit
  def edit
    @countries = Country.all
    @states = State.all
  end

  # POST /sales_orders
  # POST /sales_orders.json
  def create
    @sales_order = SalesOrder.new(sales_order_params)
    @countries = Country.all

    respond_to do |format|
      if @sales_order.save
        format.html { redirect_to @sales_order, notice: 'Sales order was successfully created.' }
        format.json { render :show, status: :created, location: @sales_order }
      else
        format.html { render :new }
        format.json { render json: @sales_order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sales_orders/1
  # PATCH/PUT /sales_orders/1.json
  def update
    respond_to do |format|
      if @sales_order.update(sales_order_params)
        format.html { redirect_to @sales_order, notice: 'Sales order was successfully updated.' }
        format.json { render :show, status: :ok, location: @sales_order }
      else
        format.html { render :edit }
        format.json { render json: @sales_order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sales_orders/1
  # DELETE /sales_orders/1.json
  def destroy
    @sales_order.destroy
    respond_to do |format|
      format.html { redirect_to sales_orders_url, notice: 'Sales order was successfully removed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sales_order
      @sales_order = SalesOrder.with_deleted.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sales_order_params
      params.require(:sales_order).permit(:customer_id, :payment_schedule_id, :sales_order_status_id, :sales_person_id, :order_date, :order_grand_total, :order_due_in_full_date, :is_approved, :order_balance_due, :order_balance_received, :deleted_at, sales_order_lines_attributes: [:id, :product_qty_id, :qty_requested, :qty_approved, :unit_price, :product_total, :product_id, :_destroy])
    end

    # Create params for which column to sort, only allowing columns from sales_order
    def sort_column
      ['status_desc', 'customer_name', 'person', 'is_approved', 'order_date', 'order_due_in_full_date', 'order_grand_total',
            'order_balance_due', 'order_balance_received'].include?(params[:sort]) ? params[:sort] : 'order_date'
    end

    # Create param for column sort direction, only allowing 'asc' or 'desc' values
    def sort_direction
      %w[asc desc].include?(params[:direction]) ? params[:direction] : 'desc'
    end
end
