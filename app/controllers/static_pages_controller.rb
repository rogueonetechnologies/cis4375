class StaticPagesController < ApplicationController

  def home
    @outstanding = State.joins(:sales_orders).select(:state_name, 'SUM(order_grand_total) AS sales_amt', 'SUM(order_balance_due) AS sales_due', 'SUM(order_balance_received) AS sales_rec').where('extract(year  from order_date) = ? AND is_approved = ?', Date.today.year, true).group(:state_name)
    respond_to do |format|
      format.html
      format.xlsx {
        response.headers['Content-Disposition'] = 'attachment; filename="ORCA_YTD_Outstanding_'+Date.today.to_s+'.xlsx"'
      }
    end
  end

end