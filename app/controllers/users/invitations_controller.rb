class Users::InvitationsController < Devise::InvitationsController
  before_action :configure_permitted_parameters

  def after_invite_path_for(resource)
    users_path
  end

  protected
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:invite, keys: [:admin])
  end
end