class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy, :hard_delete, :restore]
  before_action 'authenticate_admin!', only: [:index, :new, :create, :destroy, :hard_delete, :restore, :deleted]

  # GET /users
  # GET /users.json
  def index
    @users = User.paginate(:page => params[:page])
  end

  # GET /user/deleted
  def deleted
    @users = User.only_deleted.order(email: :asc).paginate(:page => params[:page])
  end

  # POST /user/:id/delete
  def hard_delete
    @user.really_destroy!
    redirect_to deleted_users_path, notice: 'User permanently deleted'
  end

  # POST /user/:id/reinstate
  def restore
    @user.restore
    redirect_to deleted_users_path, notice: 'User reinstated'
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully removed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.with_deleted.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :password, :admin)
    end
end
