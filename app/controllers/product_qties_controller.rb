class ProductQtiesController < ApplicationController
  before_action :set_product_qty, only: [:show, :edit, :update, :destroy, :hard_delete, :restore]
  before_action :authenticate_admin!, only: [:hard_delete, :restore, :deleted]

  # GET /product_qties
  # GET /product_qties.json
  def index
    @product_qties = ProductQty.paginate(:page => params[:page])
  end

  # GET /product_qty/deleted
  def deleted
    @product_qties = ProductQty.only_deleted.order(product_id: :asc, qty_type_id: :asc).paginate(:page => params[:page])
  end

  # POST /product_qty/:id/delete
  def hard_delete
    @product_qty.really_destroy!
    redirect_to deleted_product_qties_path, notice: 'Product Quantity permanently deleted'
  end

  # POST /product_qty/:id/reinstate
  def restore
    @product_qty.restore
    redirect_to deleted_product_qties_path, notice: 'Product Quantity reinstated'
  end

  # GET /product_qties/1
  # GET /product_qties/1.json
  def show
  end

  # GET /product_qties/new
  def new
    @product_qty = ProductQty.new
  end

  # GET /product_qties/1/edit
  def edit
  end

  # POST /product_qties
  # POST /product_qties.json
  def create
    @product_qty = ProductQty.new(product_qty_params)

    respond_to do |format|
      if @product_qty.save
        format.html { redirect_to @product_qty, notice: 'Product qty was successfully created.' }
        format.json { render :show, status: :created, location: @product_qty }
      else
        format.html { render :new }
        format.json { render json: @product_qty.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /product_qties/1
  # PATCH/PUT /product_qties/1.json
  def update
    respond_to do |format|
      if @product_qty.update(product_qty_params)
        format.html { redirect_to @product_qty, notice: 'Product qty was successfully updated.' }
        format.json { render :show, status: :ok, location: @product_qty }
      else
        format.html { render :edit }
        format.json { render json: @product_qty.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /product_qties/1
  # DELETE /product_qties/1.json
  def destroy
    @product_qty.destroy
    respond_to do |format|
      format.html { redirect_to product_qties_url, notice: 'Product qty was successfully removed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product_qty
      @product_qty = ProductQty.with_deleted.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_qty_params
      params.require(:product_qty).permit(:product_id, :qty_type_id, :description, :deleted_at)
    end
end
