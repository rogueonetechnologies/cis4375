class ContactsController < ApplicationController

  before_action :set_contact, only: [:show, :edit, :update, :destroy, :hard_delete, :restore]
  before_action :authenticate_user!
  before_action :authenticate_admin!, only: [:deleted, :hard_delete, :restore]



  # GET /contacts
  # GET /contacts.json
  def index
    @contacts = Contact.joins(:customer).order('customers.customer_name ASC', contact_name: :asc).paginate(:page => params[:page])
    @all_contacts = Contact.all
    respond_to do |format|
      format.html
      format.xlsx {
        response.headers['Content-Disposition'] = 'attachment; filename="ORCA_Contact_List_'+Date.today.to_s+'.xlsx"'
      }
    end
  end

  def deleted
    @contacts = Contact.only_deleted.paginate(:page => params[:page])
  end

  # POST /contact/:id/delete
  def hard_delete
    @contact.really_destroy!
    redirect_to deleted_contacts_path, notice: 'Contact permanently deleted'
  end

  # POST /contact/:id/reinstate
  def restore
    @contact.restore
    redirect_to deleted_contacts_path, notice: 'Contact reinstated'
  end

  # GET /contacts/1
  # GET /contacts/1.json
  def show
  end

  # GET /contacts/new
  def new
    @contact = Contact.new
  end

  # GET /contacts/1/edit
  def edit
  end

  # POST /contacts
  # POST /contacts.json
  def create
    @contact = Contact.new(contact_params)

    respond_to do |format|
      if @contact.save
        format.html { redirect_to @contact, notice: 'Contact was successfully created.' }
        format.json { render :show, status: :created, location: @contact }
      else
        format.html { render :new }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /contacts/1
  # PATCH/PUT /contacts/1.json
  def update
    respond_to do |format|
      if @contact.update(contact_params)
        format.html { redirect_to @contact, notice: 'Contact was successfully updated.' }
        format.json { render :show, status: :ok, location: @contact }
      else
        format.html { render :edit }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /contacts/1
  # DELETE /contacts/1.json
  def destroy
    @contact.destroy
    respond_to do |format|
      format.html { redirect_to contacts_url, notice: 'Contact was successfully removed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contact
      @contact = Contact.with_deleted.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contact_params
      params.require(:contact).permit(:customer_id, :contact_name, :phone_number, :email)
    end
end
