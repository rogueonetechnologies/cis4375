class CustomersController < ApplicationController
  before_action :set_customer, only: [:show, :edit, :update, :destroy, :hard_delete, :restore, :ajax]
  before_action :authenticate_admin!, only: [:deleted, :hard_delete, :restore]

  # GET /customers
  # GET /customers.json
  def index
    @customers = Customer.order(customer_name: :asc).paginate(:page => params[:page])
    @all_customers = Customer.all
    respond_to do |format|
      format.html
      format.xlsx {
        response.headers['Content-Disposition'] = 'attachment; filename="ORCA_Customer_List_'+Date.today.to_s+'.xlsx"'
      }
    end
  end

  # GET /customer/deleted
  def deleted
    @customers = Customer.only_deleted.paginate(:page => params[:page])
  end

  #GET /customer/:id/ajax
  def ajax
    respond_to do |format|
      format.html { render :layout => false } # your-action.html.erb
    end
  end

  # POST /customer/:id/delete
  def hard_delete
    @customer.really_destroy!
    redirect_to deleted_customers_path, notice: 'Customer permanently deleted'
  end

  # POST /customer/:id/reinstate
  def restore
    @customer.restore(:recursive => true)
    redirect_to deleted_customers_path, notice: 'Customer reinstated'
  end

  # GET /customers/1
  # GET /customers/1.json
  def show
    @sales_orders = SalesOrder.where(:customer_id => @customer.id).paginate(:page => params[:page], :per_page => 10)
  end

  # GET /customers/new
  def new
    @countries = Country.joins(:states).distinct
    @customer = Customer.new
  end

  # GET /customers/1/edit
  def edit
    @countries = Country.joins(:states).distinct
  end

  # POST /customers
  # POST /customers.json
  def create
    @countries = Country.joins(:states).group('countries.id')
    @customer = Customer.new(customer_params)

    respond_to do |format|
      if @customer.save
        format.html { redirect_to @customer, notice: 'Customer was successfully created.' }
        format.json { render :show, status: :created, location: @customer }
      else
        format.html { render :new }
        format.json { render json: @customer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /customers/1
  # PATCH/PUT /customers/1.json
  def update
    respond_to do |format|
      if @customer.update(customer_params)
        format.html { redirect_to @customer, notice: 'Customer was successfully updated.' }
        format.json { render :show, status: :ok, location: @customer }
      else
        format.html { render :edit }
        format.json { render json: @customer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /customers/1
  # DELETE /customers/1.json
  def destroy
    @customer.destroy
    respond_to do |format|
      format.html { redirect_to customers_url, notice: 'Customer was successfully removed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_customer
      @customer = Customer.with_deleted.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def customer_params
      params.require(:customer).permit(:customer_status_id, :state_id, :country_id, :customer_name, :phone_number, contacts_attributes: [:id, :contact_name, :phone_number, :email, :_destroy])
    end
end
