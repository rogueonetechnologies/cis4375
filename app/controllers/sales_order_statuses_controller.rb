class SalesOrderStatusesController < ApplicationController
  before_action :set_sales_order_status, only: [:show, :edit, :update, :destroy, :hard_delete, :restore]
  before_action 'authenticate_admin!'

  # GET /sales_order_statuses
  # GET /sales_order_statuses.json
  def index
    @sales_order_statuses = SalesOrderStatus.paginate(:page => params[:page])
  end

  # GET /sales_order_status/deleted
  def deleted
    @sales_order_statuses = SalesOrderStatus.only_deleted.order(status_desc: :asc).paginate(:page => params[:page])
  end

  # POST /sales_order_status/:id/delete
  def hard_delete
    @sales_order_status.really_destroy!
    redirect_to deleted_sales_order_statuses_path, notice: 'Sales Order Status permanently deleted'
  end

  # POST /sales_order_status/:id/reinstate
  def restore
    @sales_order_status.restore
    redirect_to deleted_sales_order_statuses_path, notice: 'Sales Order Status reinstated'
  end

  # GET /sales_order_statuses/1
  # GET /sales_order_statuses/1.json
  def show
  end

  # GET /sales_order_statuses/new
  def new
    @sales_order_status = SalesOrderStatus.new
  end

  # GET /sales_order_statuses/1/edit
  def edit
  end

  # POST /sales_order_statuses
  # POST /sales_order_statuses.json
  def create
    @sales_order_status = SalesOrderStatus.new(sales_order_status_params)

    respond_to do |format|
      if @sales_order_status.save
        format.html { redirect_to @sales_order_status, notice: 'Sales Order status was successfully created.' }
        format.json { render :show, status: :created, location: @sales_order_status }
      else
        format.html { render :new }
        format.json { render json: @sales_order_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sales_order_statuses/1
  # PATCH/PUT /sales_order_statuses/1.json
  def update
    respond_to do |format|
      if @sales_order_status.update(sales_order_status_params)
        format.html { redirect_to @sales_order_status, notice: 'Sales Order status was successfully updated.' }
        format.json { render :show, status: :ok, location: @sales_order_status }
      else
        format.html { render :edit }
        format.json { render json: @sales_order_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sales_order_statuses/1
  # DELETE /sales_order_statuses/1.json
  def destroy
    @sales_order_status.destroy
    respond_to do |format|
      format.html { redirect_to sales_order_statuses_url, notice: 'Sales Order status was successfully removed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sales_order_status
      @sales_order_status = SalesOrderStatus.with_deleted.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sales_order_status_params
      params.require(:sales_order_status).permit(:status_desc, :deleted_at)
    end
end
