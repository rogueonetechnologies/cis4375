class PaymentsController < ApplicationController
  before_action :set_payment, only: [:show, :edit, :update, :destroy, :hard_delete, :restore]
  before_action :authenticate_admin!, only: [:deleted, :hard_delete, :restore]

  helper_method :get_customer_from_so, :customers_with_credit, :get_so_from_so

  # GET /payments
  # GET /payments.json
  def index
    @q = Payment.ransack(params[:q])
    @payments = @q.result.paginate(:page => params[:page])
    @all_payments = Payment.all
    respond_to do |format|
      format.html
      format.xlsx {
        response.headers['Content-Disposition'] = 'attachment; filename="ORCA_Payments_List_'+Date.today.to_s+'.xlsx"'
      }
    end
  end

  # GET /payment/deleted
  def deleted
    @payments = Payment.only_deleted.paginate(:page => params[:page])
  end

  # POST /payment/:id/delete
  def hard_delete
    @payment.really_destroy!
    redirect_to deleted_payments_path, notice: 'Payment permanently deleted'
  end

  # POST /payment/:id/reinstate
  def restore
    @payment.restore
    redirect_to deleted_payments_path, notice: 'Payment reinstated'
  end

  # GET /payments/1
  # GET /payments/1.json
  def show
  end

  # GET /payments/new
  def new
    @payment = Payment.new
    # @customers = Customer.all
    @so = -1
    if params[:so]
      @so = params[:so]
    end
  end

  # GET /payments/1/edit
  def edit
    @so = -1
    if params[:so]
      @so = params[:so]
    end
  end

  # POST /payments
  # POST /payments.json
  def create
    @payment = Payment.new(payment_params)

    respond_to do |format|
      if @payment.save
        format.html { redirect_to @payment, notice: 'Payment was successfully created.' }
        format.json { render :show, status: :created, location: @payment }
      else
        format.html { render :new }
        format.json { render json: @payment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /payments/1
  # PATCH/PUT /payments/1.json
  def update
    respond_to do |format|
      if @payment.update(payment_params)
        format.html { redirect_to @payment, notice: 'Payment was successfully updated.' }
        format.json { render :show, status: :ok, location: @payment }
      else
        format.html { render :edit }
        format.json { render json: @payment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /payments/1
  # DELETE /payments/1.json
  def destroy
    @payment.destroy
    respond_to do |format|
      format.html { redirect_to payments_url, notice: 'Payment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # helper methods
  def get_customer_from_so(so)
    SalesOrder.find(so).customer.customer_name
  end
  def get_so_from_so(so)
    SalesOrder.find(so)
  end
  def customers_with_credit
    Customer.select(:id, :customer_name, 'SUM(order_balance_due)', :is_approved).joins(:sales_orders).having('SUM(order_balance_due) > 0.01 AND is_approved = true').group(:id, :customer_name, :is_approved).distinct
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_payment
      @payment = Payment.with_deleted.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def payment_params
      params.require(:payment).permit(:payment_type_id, :sales_order_id, :purchase_order_id, :payment_date, :payment_amt)
    end
end
