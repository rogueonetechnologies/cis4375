class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy, :hard_delete, :restore]
  before_action :authenticate_admin!, only: [:hard_delete, :deleted, :restore]

  # GET /products
  # GET /products.json
  def index
    @products = Product.order(product_name: :asc).paginate(:page => params[:page])
    @all_prods = Product.all
    respond_to do |format|
      format.html
      format.xlsx {
        response.headers['Content-Disposition'] = 'attachment; filename="ORCA_Product_List_'+Date.today.to_s+'.xlsx"'
      }
    end
  end

  # GET /product/deleted
  def deleted
    @products = Product.only_deleted.paginate(:page => params[:page])
  end

  # POST /product/:id/delete
  def hard_delete
    @product.really_destroy!
    redirect_to deleted_products_path, notice: 'Product permanently deleted'
  end

  # POST /product/:id/reinstate
  def restore
    @product.restore(:recursive => true)
    redirect_to deleted_products_path, notice: 'Product reinstated'
  end

  # GET /products/1
  # GET /products/1.json
  def show
  end

  # GET /products/new
  def new
    @product = Product.new
  end

  # GET /products/1/edit
  def edit
  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new(product_params)

    respond_to do |format|
      if @product.save
        format.html { redirect_to @product, notice: 'Product was successfully created.' }
        format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update
    respond_to do |format|
      if @product.update(product_params)
        format.html { redirect_to @product, notice: 'Product was successfully updated.' }
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_url, notice: 'Product was successfully removed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.with_deleted.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.require(:product).permit(:product_name, product_qties_attributes: [:id, :qty_type_id, :description, :_destroy])
    end
end