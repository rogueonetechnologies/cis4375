# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

jQuery ->
  $(document).on 'turbolinks:load', ->
    $('#sales_person_state_id').parent().hide()
    states = $('#sales_person_state_id').html()
    $('#sales_person_country_id').change ->
      country = $('#sales_person_country_id :selected').text()
      options = $(states).filter("optgroup[label='#{country}']").html()
      if options
        $('#sales_person_state_id').html(options).parent().show()
      else
        $('#sales_person_state_id').empty().parent().hide()