# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

jQuery ->
  $('#sales_order_lines').bind 'cocoon:after-insert', (e, sales_order_lines) ->
    product_qties = $('sales_order_lines#product_qty_id').html()
    $('sales_order_lines#product_id').change ->
      product = $('sales_order_lines#product_id :selected').text()
      options = $(product_qties).filter("optgroup[label='#{product}']").html()
      if options
        $('sales_order_lines#product_qty_id').html(options)
      else
        $('sales_order_lines#product_qty_id').empty()