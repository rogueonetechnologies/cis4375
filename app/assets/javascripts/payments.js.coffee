# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'turbolinks:load', ->

  # datepicker

  $('.datepicker').datepicker({
    format: "dd/mm/yyyy",
    todayBtn: "linked",
    orientation: "auto-left"
  })

  # filter sales orders by customer

  customers = $('#customer').html()
  sales_orders = $('#payment_sales_order_id').html()
  $('#customer').change ->
    customer_sel = $('#customer :selected').text()
    options = $(sales_orders).filter('optgroup[label="' + customer_sel + '"]').html()
    if options
      $('#payment_sales_order_id').html(options)
      $("#payment_sales_order_id").trigger("change")
    else
      $('#payment_sales_order_id').empty()

  # clear filter
  $('#clear_sales_order_filter').click ->
    $('#customer').html(customers)
    $('#payment_sales_order_id').html(sales_orders)
    $(".calculated").hide()

  # show order_balance_due for selected Sales Order
  $('#payment_sales_order_id').change ->
    so_id = $('#payment_sales_order_id :selected').val()
    console.log "/sales_order/#{so_id}/ajax"
    $.ajax
      url: "/sales_order/#{so_id}/ajax"
      dataType: "html"
      error: (jqXHR, textStatus, errorThrown) ->
        $('body').append "AJAX Error: #{textStatus}"
      success: (data, textStatus, jqXHR) ->
        $('.so-balance-due').html data
    $('.so-balance-due').show()


#    insert remaining balance into payment amt
  $('#remainder').click ->
    console.log $('#so_credit').attr("number")
    $('#payment_payment_amt').val $('#so_credit').attr("number")