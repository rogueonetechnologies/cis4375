# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'turbolinks:load', ->

#  Make datepicker
  $('#datepicker').datepicker({
    format: "dd/mm/yyyy",
    todayBtn: "linked",
    orientation: "auto-left"
  })

  $('#sales_order_lines').on 'cocoon:after-insert', (e, added_line) ->

#    Filter product sizes by product
    #    Get the qties select
    product_qties = added_line.find('[id^="sales_order_sales_order_lines_"][id$="_product_qty_id"]').html()
#    Fire change event on products select
    added_line.find('[id^="sales_order_sales_order_lines_"][id$="_product_id"]').change ->
#      Get selected element in product select
      product_sel = added_line.find('[id^="sales_order_sales_order_lines_"][id$="_product_id"] :selected').text()
#      Get all options for the selected product's optgroup
      options = $(product_qties).filter("optgroup[label='#{product_sel}']").html()
#      Enable the qties select
      added_line.find('[id^="sales_order_sales_order_lines_"][id$="_product_qty_id"]').attr('disabled', false)
#      change qties select to only the options related to the selected product, or empty if the product doesn't have any qties
      if options
        added_line.find('[id^="sales_order_sales_order_lines_"][id$="_product_qty_id"]').html(options)
      else
        added_line.find('[id^="sales_order_sales_order_lines_"][id$="_product_qty_id"]').empty()
        

    #    add up product total amounts upon changing qty requested
    added_line.find('[id^="sales_order_sales_order_lines_"][id$="_qty_requested"]').change ->
      approved = added_line.find('[id^="sales_order_sales_order_lines_"][id$="_qty_approved"]').val()
      if approved == ''
        price = added_line.find('[id^="sales_order_sales_order_lines_"][id$="_unit_price"]').val()
        if price > 0 && price != ''
          requested = added_line.find('[id^="sales_order_sales_order_lines_"][id$="_qty_requested"]').val()
          added_line.find('[id^="sales_order_sales_order_lines_"][id$="_product_total"]').val Math.round((requested * price) * 100) / 100

    #    add up product total amounts upon changing unit price
    added_line.find('[id^="sales_order_sales_order_lines_"][id$="_unit_price"]').change ->
      requested = added_line.find('[id^="sales_order_sales_order_lines_"][id$="_qty_requested"]').val()
      price = added_line.find('[id^="sales_order_sales_order_lines_"][id$="_unit_price"]').val()
      approved = added_line.find('[id^="sales_order_sales_order_lines_"][id$="_qty_approved"]').val()

      if approved >= 0 && approved != ''
        amt = approved
      else if requested > 0 && requested != ''
        amt = requested
      added_line.find('[id^="sales_order_sales_order_lines_"][id$="_product_total"]').val Math.round((amt * price) * 100) / 100

    #    add up product total amounts upon changing qty approved
    added_line.find('[id^="sales_order_sales_order_lines_"][id$="_qty_approved"]').change ->
      price = added_line.find('[id^="sales_order_sales_order_lines_"][id$="_unit_price"]').val()
      if price > 0 && price != ''
        approved = added_line.find('[id^="sales_order_sales_order_lines_"][id$="_qty_approved"]').val()
        if approved > 0 && approved != ''
          added_line.find('[id^="sales_order_sales_order_lines_"][id$="_product_total"]').val Math.round((approved * price) * 100) / 100
        else
          requested = added_line.find('[id^="sales_order_sales_order_lines_"][id$="_qty_requested"]').val()
          added_line.find('[id^="sales_order_sales_order_lines_"][id$="_product_total"]').val Math.round((requested * price) * 100) / 100


#  filter customer and salesperson dropdowns by state
  states = $('#state').html()
  customers = $('#sales_order_customer_id').html()
  salesppl = $('#sales_order_sales_person_id').html()
  $('#country').change ->
    country_sel = $('#country :selected').text()
    options = $(states).filter("optgroup[label='#{country_sel}']").html()
    if options
      $('#state').html(options)
      $('#state').trigger("change")
    else
      $('#state').empty()
      $('#sales_order_customer_id').empty()
      $('#sales_order_sales_person_id').empty()

  $('#state').change ->
    state_sel = $('#state :selected').text()
    custOpts = $(customers).filter("optgroup[label='#{state_sel}']").html()
    pplOpts = $(salesppl).filter("optgroup[label='#{state_sel}']").html()
    if custOpts
      $('#sales_order_customer_id').html(custOpts)
      $("#sales_order_customer_id").trigger("change")
    else
      $('#sales_order_customer_id').empty()

    if pplOpts
      $('#sales_order_sales_person_id').html(pplOpts)
    else
      $('#sales_order_sales_person_id').empty()

  $('#clear_customer_filter').click ->
    $('#country').val(null)
    $('#state').html(states)
    $('#sales_order_customer_id').html(customers)
    $('#sales_order_sales_person_id').html(salesppl)
    $(".calculated").hide()

  #  show calculated fields depending on chosen customer
  $('#sales_order_customer_id').change ->
    cust_id = $('#sales_order_customer_id :selected').val()
    console.log "/customer/#{cust_id}/ajax"
    $.ajax
      url: "/customer/#{cust_id}/ajax"
      dataType: "html"
      error: (jqXHR, textStatus, errorThrown) ->
        $('body').append "AJAX Error: #{textStatus}"
      success: (data, textStatus, jqXHR) ->
        $('.calculated').html data
    $('.calculated').show()

#  show potential results of making the sale
  $('#run_sim').click ->
#    show simulation section
    $('#simulation').show()

#    get order total
    array = $('.product_amt')
    total = 0
    total = total + parseFloat(product.value) for product in array
    $('#sales_order_order_grand_total').val Math.round(total * 100) / 100

#    find due date
    date_str = $('#datepicker').val()
    length = $('#sales_order_payment_schedule_id :selected').text()
    if date_str != '' && length != ''
      pieces = date_str.split("/")
      date = new Date(parseInt(pieces[2]), parseInt(pieces[1])-1, parseInt(pieces[0]))
      date.setDate(date.getDate() + parseInt(length));
      $('#order_due').val ("0" + date.getDate()).slice(-2) + "/" + ("0" + parseInt(date.getMonth()+1)).slice(-2) + "/" + date.getFullYear()

#    get potential credit amount
    existing = $('#cust_credit').val()
    console.log existing
    more = $('#sales_order_order_grand_total').val()
    console.log more
    console.log parseFloat(existing) + parseFloat(more)
    sum = parseFloat(existing) + parseFloat(more)
    $('#potential_credit').val Math.round(sum * 100) / 100

#     code for modal
    $("#myModal").modal('show')

  if $('#sales_order_customer_id :selected').text()
    $('#sales_order_customer_id :selected').trigger("change")

  #    code to change download params in ytd_sales
  if parseInt($('#q_customer_state_id_eq :selected').val()) > 0
    chosen = $('#q_customer_state_id_eq :selected').val()
    console.log chosen
    link = $('#download').attr('href')
    console.log link
    newLink = link + "?state=" + chosen
    console.log newLink
    $('#download').attr('href', newLink)
    console.log $('#download').attr('href')