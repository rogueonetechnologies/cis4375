json.extract! sales_order, :id, :customer_id, :payment_schedule_id, :sales_order_status_id, :sales_person_id, :order_date, :order_grand_total, :order_due_in_full_date, :is_approved, :order_balance_due, :order_balance_received, :deleted_at, :created_at, :updated_at
json.url sales_order_url(sales_order, format: :json)
