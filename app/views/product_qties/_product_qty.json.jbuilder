json.extract! product_qty, :id, :product_id, :qty_type_id, :description, :deleted_at, :created_at, :updated_at
json.url product_qty_url(product_qty, format: :json)