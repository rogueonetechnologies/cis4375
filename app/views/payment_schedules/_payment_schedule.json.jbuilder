json.extract! payment_schedule, :id, :schedule_length, :deleted_at, :created_at, :updated_at
json.url payment_schedule_url(payment_schedule, format: :json)
