json.extract! customer, :id, :customer_status_id, :state_id, :customer_type_id, :customer_name, :phone_number, :created_at, :updated_at
json.url customer_url(customer, format: :json)