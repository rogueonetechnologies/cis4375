json.extract! sales_person, :id, :state_id, :first_name, :last_name, :created_at, :updated_at
json.url sales_person_url(sales_person, format: :json)