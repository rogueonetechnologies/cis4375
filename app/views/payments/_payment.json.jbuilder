json.extract! payment, :id, :payment_type_id, :purchase_order_id, :payment_date, :payment_amt, :created_at, :updated_at
json.url payment_url(payment, format: :json)