json.extract! qty_type, :id, :qty_type_name, :deleted_at, :created_at, :updated_at
json.url qty_type_url(qty_type, format: :json)