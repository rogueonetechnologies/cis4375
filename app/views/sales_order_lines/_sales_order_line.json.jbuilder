json.extract! sales_order_line, :id, :product_id, :qty_requested, :qty_approved, :unit_price, :product_total, :deleted_at, :created_at, :updated_at
json.url sales_order_line_url(sales_order_line, format: :json)
