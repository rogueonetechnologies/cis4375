json.extract! sales_order_status, :id, :status_desc, :deleted_at, :created_at, :updated_at
json.url sales_order_status_url(sales_order_status, format: :json)
