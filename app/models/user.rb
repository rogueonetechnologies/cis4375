class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable,
         :recoverable, :trackable, :validatable, :invite_for => 1.week

  acts_as_paranoid

  # validates :email, length: { maximum: 50, too_long:
  #     "%{count} is the maximum characters allowed", minimum: 8 }
  # validates :password, length: { maximum: 50, too_long:
  #     "%{count} is the maximum characters allowed", minimum: 5 }
end
