class CustomerStatus < ActiveRecord::Base
  acts_as_paranoid

  has_many :customers

  validates :status_desc, presence: true
  validates :status_desc, length: { maximum: 50, too_long: "%{count} characters is the maximum allowed"}
end
