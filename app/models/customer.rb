class Customer < ActiveRecord::Base
  acts_as_paranoid

  has_many :contacts, :dependent => :destroy
  accepts_nested_attributes_for :contacts, reject_if: :all_blank, allow_destroy: true
  has_many :sales_orders
  has_many :sales_order_lines, through: :sales_orders
  has_many :payments, through: :sales_orders

  belongs_to :customer_status
  belongs_to :state
  belongs_to :country

  accepts_nested_attributes_for :contacts

  validates :country, :state, :customer_status, :customer_name, :phone_number, :presence => true
  validates :customer_name, length: {maximum: 50, too_long: "%{count} characters is the maximum allowed"}
  VALID_PHONE_NUMBER_REGEX = /\A[\d-]*\d[\d-]*\z/
  validates :phone_number, :format => {:with => VALID_PHONE_NUMBER_REGEX}, :length => {:minimum => 7, :maximum => 15}

  @customers = Customer.all

  def sales_orders_with_balance
    self.sales_orders.where(:sales_order_status_id => [1, 2])
  end

  def credit
    sum = 0
    self.sales_orders.each do |so|
      if so.is_approved
        sum += so.order_balance_due
      end
    end
    sum
  end

  def sos
    self.sales_orders.length
  end

end