class SalesOrderLine < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :sales_order, :dependent => :destroy
  belongs_to :product_qty
  belongs_to :product

  # only validate sales order on update because it is a nested field in Sales Order
  validates :sales_order, :presence => true, :on => :update

  validates :product_qty, :product, :qty_requested, :qty_approved, :unit_price, :product_total, :presence => true
  validates :qty_requested, :qty_approved, :numericality => {:only_integer => true, :greater_than_or_equal_to => 0}
  validates :unit_price, :numericality => true

  before_validation do
    if self.qty_approved == nil
      self.qty_approved = self.qty_requested
    end
  end

end