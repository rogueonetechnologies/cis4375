class Payment < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :payment_type
  belongs_to :sales_order

  validates :sales_order, :payment_date, :payment_amt, :presence => true
  validates :payment_amt, :numericality => {:greater_than_or_equal_to => 0}
  validate :payment_amt_must_be_less_than_or_equal_to_order_balance_due

  def payment_amt_must_be_less_than_or_equal_to_order_balance_due
    payment_max = self.sales_order.order_balance_due
    self.errors.add(:payment_amt, "Payment must be less than the amount due") unless self.payment_amt <= payment_max
  end

  before_restore :restore_callback

  def restore_callback

    self.sales_order.order_balance_due -= self.payment_amt
    self.sales_order.order_balance_received += self.payment_amt

    if Integer(self.payment_amt * 100) == Integer(self.sales_order.order_balance_due * 100)
      self.sales_order.sales_order_status_id = 3
    elsif self.sales_order.sales_order_status_id == 1
      self.sales_order.sales_order_status_id = 2
    elsif Integer(self.sales_order.order_balance_due * 100) == 0
      self.sales_order.sales_order_status_id = 3
    end

    self.sales_order.save
  end

  after_restore do
    if Integer(self.sales_order.order_balance_received * 100) > Integer(self.sales_order.order_grand_total * 100)
      self.destroy
    elsif Integer(self.sales_order.order_balance_due * 100) == 0
      pmt = self.sales_order.latest_payment
      logger.info pmt
      pmt.payment_type_id = 3
      logger.info pmt.payment_type_id
      pmt.save
    end
  end

  before_save do
    if Integer(self.payment_amt * 100) == Integer(self.sales_order.order_grand_total * 100)
      self.payment_type_id = 2
    elsif Integer(self.payment_amt * 100) == Integer(self.sales_order.order_balance_due * 100)
      self.payment_type_id = 3
    else
      self.payment_type_id = 1
    end
  end

  after_save do
    if Integer(self.payment_amt * 100) == Integer(self.sales_order.order_balance_due * 100)
      self.sales_order.sales_order_status_id = 3
      self.sales_order.save
    elsif self.sales_order.sales_order_status_id == 1
      self.sales_order.sales_order_status_id = 2
      self.sales_order.save
    end
  end

  after_destroy do
    self.sales_order.order_balance_due += self.payment_amt
    self.sales_order.order_balance_received -= self.payment_amt

    if Integer(self.sales_order.order_balance_received * 100) == 0
      self.sales_order.sales_order_status_id = 1
    else
      self.sales_order.sales_order_status_id = 2
    end

    self.sales_order.payments.each do |pmt|
      if pmt.payment_type_id == 3
        pmt.payment_type_id == 1
        pmt.save
      end
    end

    self.sales_order.save

  end

  trigger.after(:insert) do
      "UPDATE sales_orders SET order_balance_received = order_balance_received + NEW.payment_amt WHERE sales_orders.id = NEW.sales_order_id"
  end
  trigger.after(:insert) do
    "UPDATE sales_orders SET order_balance_due = order_balance_due - NEW.payment_amt WHERE sales_orders.id = NEW.sales_order_id"
  end

  trigger.after(:update).of(:payment_amt) do
    "UPDATE sales_orders SET order_balance_received = order_balance_received + ( NEW.payment_amt - OLD.payment_amt ) WHERE sales_orders.id = NEW.sales_order_id"
  end

  trigger.after(:update).of(:payment_amt) do
    "UPDATE sales_orders SET order_balance_due = order_balance_due - ( NEW.payment_amt - OLD.payment_amt ) WHERE sales_orders.id = NEW.sales_order_id"
  end

  trigger.after(:destroy) do
    "UPDATE sales_orders SET order_balance_received = order_balance_received - OLD.payment_amt WHERE sales_orders.id = OLD.sales_order_id"
  end

  trigger.after(:destroy) do
    "UPDATE sales_orders SET order_balance_due = order_balance_due + OLD.payment_amt WHERE sales_orders.id = OLD.sales_order_id"
  end
end
