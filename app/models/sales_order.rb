class SalesOrder < ActiveRecord::Base
  acts_as_paranoid

  has_many :payments
  has_many :sales_order_lines, inverse_of: :sales_order
  accepts_nested_attributes_for :sales_order_lines, reject_if: :all_blank, allow_destroy: true

  belongs_to :sales_order_status
  belongs_to :customer
  belongs_to :state
  belongs_to :sales_person
  belongs_to :payment_schedule

  validates :customer, :sales_order_status, :sales_person, :payment_schedule, :order_date, :order_grand_total, :order_due_in_full_date, :order_balance_received, :presence => true

  validate :must_have_lines

  def must_have_lines
    if self.sales_order_lines.length == 0
      errors.add(:sales_order_lines, "You must include at least one line on a sales order")
    end
  end

  trigger.after(:insert) do
    "UPDATE sales_orders SET order_balance_due = order_grand_total WHERE sales_orders.id = NEW.id"
  end

  before_save do
    if !self.is_approved
      self.sales_order_status_id = 4
    end
  end

  def latest_payment
    self.payments.order(payment_date: :asc).first
  end

end
