class State < ActiveRecord::Base
  acts_as_paranoid

  has_many :customers
  has_many :sales_orders, :through => :customers
  has_many :sales_people
  has_many :contacts, through: :customers
  has_many :sales_orders, through: :sales_people

  belongs_to :country

  validates :country, :state_name, :state_abbv, :presence => true
  validates :state_name, length: { maximum: 50, too_long: "%{count} is the maximum characters allowed"}
  validates :state_abbv, length: { maximum: 5, too_long: "%{count} is the maximum characters allowed"}
end
