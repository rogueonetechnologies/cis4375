class Contact < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :customer

  # Only validate customer on update because inserting as a nested field with customer fails when customer is required
  validates :customer, :presence => true, :on => :update

  validates :contact_name, :phone_number, :presence => true
  validates :contact_name, length: { maximum: 50, too_long: "%{count} characters is the maximum allowed"}

  # Only allow valid phone number formats using regular expression
  VALID_PHONE_NUMBER_REGEX = /\A[\d-]*\d[\d-]*\z/
  validates :phone_number, :format => { :with => VALID_PHONE_NUMBER_REGEX}, :length => { :minimum => 7, :maximum => 15}

  # Only allow valid email formats using regular expression
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email, :format => { :with => VALID_EMAIL_REGEX, :allow_blank => true}
  # allow_blank is true because email is not required per the data dictionary

end