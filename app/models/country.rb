class Country < ActiveRecord::Base
  acts_as_paranoid
  has_many :states, :dependent => :destroy
  has_many :sales_people
  has_many :customers
  has_many :contacts, through: :customers

  validates :country_name, :country_abbv, :presence => true
  validates :country_abbv, length: { maximum: 10, too_long:
      "%{count} is the maximum characters allowed", minimum: 2 }
  validates :country_abbv, length: { maximum: 10, too_long: "%{count} characters is the maximum allowed", :minimum => 2, :too_short => "must be at least %{count} characters"}

end