class SalesOrderStatus < ActiveRecord::Base

  acts_as_paranoid

  has_many :sales_orders

  validates :status_desc, :presence => true, length: {maximum: 50, too_long: "%{count} is the maximum characters allowed"}

end
