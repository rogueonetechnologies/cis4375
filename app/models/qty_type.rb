class QtyType < ActiveRecord::Base
  acts_as_paranoid
  has_many :product_qties

  validates :qty_type_name, :presence => true
  validates :qty_type_name, length: { maximum: 50, too_long: "%{count} is the maximum characters allowed"}
end
