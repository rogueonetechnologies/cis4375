class ProductQty < ActiveRecord::Base
  acts_as_paranoid
  belongs_to :product
  belongs_to :qty_type
  has_many :sales_order_lines

  validates :qty_type, :description, :presence => true
  validates :description, length: { maximum: 50, too_long: "%{count} is the maximum characters allowed"}
end
