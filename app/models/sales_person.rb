class SalesPerson < ActiveRecord::Base
  acts_as_paranoid

  has_many :sales_orders
  has_many :sales_order_lines, through: :sales_orders

  belongs_to :state
  belongs_to :country

  validates :state, :country, :first_name, :last_name, :presence => true

  VALID_NAME_REGEX = /\A[^0-9`!@#\$%\^&*+_=]+\z/
  validates :first_name, :last_name, :format => {:with => VALID_NAME_REGEX}

  validates :first_name, length: {maximum: 50, too_long: "%{count} is the maximum characters allowed"}
  validates :last_name, length: {maximum: 50, too_long: "%{count} is the maximum characters allowed"}

  def full_name
    "#{first_name} #{last_name}"
  end
end
