class PaymentType < ActiveRecord::Base
  acts_as_paranoid

  has_many :payments

  validates :type_desc, presence: true
  validates :type_desc, length: { maximum: 50, too_long: "%{count} is the maximum characters allowed"}
end
