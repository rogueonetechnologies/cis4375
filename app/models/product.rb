class Product < ActiveRecord::Base
  acts_as_paranoid

  has_many :product_qties, :dependent => :destroy
  has_many :sales_order_lines

  accepts_nested_attributes_for :product_qties, reject_if: :all_blank, allow_destroy: true


  validates :product_name, presence: true
  validates :product_name, length: { maximum: 50, too_long: "%{count} is the maximum characters allowed"}
end
