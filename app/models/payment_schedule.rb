class PaymentSchedule < ActiveRecord::Base
  acts_as_paranoid

  has_many :sales_orders

  validates :schedule_length, :presence => true, :numericality => true

  def schedule_string
    "#{schedule_length} days"
  end

end