Rails.application.routes.draw do
  # Home route
  get 'static_pages/home' => 'static_pages#home', as: :static_pages_home
  root 'static_pages#home'

    # Devise routes
  devise_for :users, :controllers => {
      invitations: 'users/invitations'
  }
  # Scaffolded routes
  resources :contacts do
    member do
      post :hard_delete
      post :restore
    end
  end
  resources :sales_people do
    member do
      post :hard_delete
      post :restore
    end
  end
  resources :countries do
    member do
      post :hard_delete
      post :restore
    end
  end
  resources :states do
    member do
      post :hard_delete
      post :restore
    end
  end
  resources :users do
    member do
      post :hard_delete
      post :restore
    end
  end
  resources :products do
    member do
      post :hard_delete
      post :restore
    end
  end
  resources :product_qties do
    member do
      post :hard_delete
      post :restore
    end
  end
  resources :qty_types do
    member do
      post :hard_delete
      post :restore
    end
  end
  resources :payments do
    member do
      post :hard_delete
      post :restore
    end
  end
  resources :payment_types do
    member do
      post :hard_delete
      post :restore
    end
  end
  resources :customer_statuses do
    member do
      post :hard_delete
      post :restore
    end
  end
  resources :customers do
    member do
      post :hard_delete
      post :restore
    end
  end
  resources :payment_schedules do
    member do
      post :hard_delete
      post :restore
    end
  end
  resources :sales_orders do
    member do
      post :hard_delete
      post :restore
    end
  end
  resources :sales_order_lines do
    member do
      post :hard_delete
      post :restore
    end
  end
  resources :sales_order_statuses do
    member do
      post :hard_delete
      post :restore
    end
  end
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  #reports routes
  get 'sales_order/ytd_sales' => 'sales_orders#ytd_sales', as: :ytd_sales

  # Customer status/credit AJAX page
  get 'customer/:id/ajax' => 'customers#ajax', as: :ajax_customer
  # Sales Order balance due for Payments Form
  get 'sales_order/:id/ajax' => 'sales_orders#ajax', as: :ajax_sales_order

  # Sales order simulation page
  post 'sales_order/simulation' => 'sales_orders#simulation', as: :sales_order_simulation


  # Hard-delete routes
  get 'customer/deleted' => 'customers#deleted', as: :deleted_customers
  get 'contact/deleted' => 'contacts#deleted', as: :deleted_contacts
  get 'sales_order/deleted' => 'sales_orders#deleted', as: :deleted_sales_orders
  get 'payment/deleted' => 'payments#deleted', as: :deleted_payments
  get 'product/deleted' => 'products#deleted', as: :deleted_products
  get 'sales_person/deleted' => 'sales_people#deleted', as: :deleted_sales_people
  get 'country/deleted' => 'countries#deleted', as: :deleted_countries
  get 'state/deleted' => 'states#deleted', as: :deleted_states
  get 'customer_status/deleted' => 'customer_statuses#deleted', as: :deleted_customer_statuses
  get 'payment_schedule/deleted' => 'payment_schedules#deleted', as: :deleted_payment_schedules
  get 'payment_type/deleted' => 'payment_types#deleted', as: :deleted_payment_types
  get 'product_qty/deleted' => 'product_qties#deleted', as: :deleted_product_qties
  get 'qty_type/deleted' => 'qty_types#deleted', as: :deleted_qty_types
  get 'sales_order_line/deleted' => 'sales_order_lines#deleted', as: :deleted_sales_order_lines
  get 'sales_order_status/deleted' => 'sales_order_statuses#deleted', as: :deleted_sales_order_statuses
  get 'user/deleted' => 'users#deleted', as: :deleted_users


  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
